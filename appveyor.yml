version: OpenLP-win-ci-b{build}

cache:
  - '%LOCALAPPDATA%\pip\Cache'
  -  /Users/appveyor/Libraries/Caches/pip

stack: python 3.8

environment:
  matrix:
    - APPVEYOR_BUILD_WORKER_IMAGE: Visual Studio 2019
      PY_DIR: C:\\Python38-x64
      CHOCO_VLC_ARG:
      FORCE_PACKAGING: 0
      FORCE_PACKAGING_MANUAL: 0
      PYICU_PACK: PyICU-2.9-cp38-cp38-win_amd64.whl
    - APPVEYOR_BUILD_WORKER_IMAGE: Visual Studio 2019
      PY_DIR: C:\\Python38
      CHOCO_VLC_ARG: --forcex86
      FORCE_PACKAGING: 0
      FORCE_PACKAGING_MANUAL: 0
      PYICU_PACK: PyICU-2.9-cp38-cp38-win32.whl
    - APPVEYOR_BUILD_WORKER_IMAGE: macos-catalina
      QT_QPA_PLATFORM: offscreen
      FORCE_PACKAGING: 0
      FORCE_PACKAGING_MANUAL: 0
      HOMEBREW_FORCE_BREWED_CURL: 1
      HOMEBREW_NO_AUTO_UPDATE: 1

init:
- cmd: set PATH=%PY_DIR%;%PY_DIR%\Scripts;%PATH%

install:
  # Update pip
  - python -m pip install --upgrade pip
  # Install generic dependencies from pypi. sqlalchemy most be 1.4 for now
  - python -m pip install "sqlalchemy<1.5" alembic appdirs chardet beautifulsoup4 lxml Mako mysql-connector-python pytest mock psycopg2-binary websockets waitress six requests QtAwesome PyQt5 PyQtWebEngine pymediainfo PyMuPDF QDarkStyle python-vlc flask-cors pytest-qt pyenchant pysword qrcode pillow "flask<2.3"
  # Install Windows only dependencies
  - cmd: python -m pip install pyodbc pypiwin32
  - cmd: choco install vlc %CHOCO_VLC_ARG% --no-progress --limit-output
  # Download and install pyicu for windows (originally from http://www.lfd.uci.edu/~gohlke/pythonlibs/)
  - cmd: python -m pip install https://get.openlp.org/win-sdk/%PYICU_PACK%
  # Mac only dependencies
  - sh: brew install --cask vlc
  - sh: brew install pkg-config icu4c
  - sh: PATH="/usr/local/opt/icu4c/bin:/usr/local/opt/icu4c/sbin:$PATH" PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/local/opt/icu4c/lib/pkgconfig" python -m pip install pyicu
  - sh: python -m pip install Pyro4 'pyobjc-core<8.2' 'pyobjc-framework-Cocoa<8.2' py-applescript

build: off

test_script:
  - ps: >-
      If (($env:APPVEYOR_REPO_TAG -eq $False)) {
        cd $env:APPVEYOR_BUILD_FOLDER
        # Run the tests
        python -m pytest tests
        # Go back to the user root folder
        cd ..
      }

after_test:
  # Only package on the master repo
  - ps: >-
      If (($env:APPVEYOR_REPO_TAG -eq $True) -Or ($env:APPVEYOR_SCHEDULED_BUILD -eq $True) -Or ($env:FORCE_PACKAGING -eq 1)) {
          # Continue on error
          $ErrorActionPreference = "Continue"
          # This is where we create a package using PyInstaller
          # Install PyInstaller
          python -m pip install --no-warn-script-location pyinstaller==4.9
          # Some windows only stuff...
          If ($isWindows) {
            # Disabled portable installers - can't figure out how to make them silent
            # - curl -L -O http://downloads.sourceforge.net/project/portableapps/PortableApps.com%20Installer/PortableApps.comInstaller_3.4.4.paf.exe
            # - PortableApps.comInstaller_3.4.4.paf.exe /S
            # - curl -L -O http://downloads.sourceforge.net/project/portableapps/PortableApps.com%20Launcher/PortableApps.comLauncher_2.2.1.paf.exe
            # - PortableApps.comLauncher_2.2.1.paf.exe /S
            # - curl -L -O http://downloads.sourceforge.net/project/portableapps/NSIS%20Portable/NSISPortable_3.0_English.paf.exe
            # - NSISPortable_3.0_English.paf.exe /S
            # Download and unpack portable-bundle
            appveyor DownloadFile https://get.openlp.org/win-sdk/portable-setup.7z
            7z x portable-setup.7z
            # Install VLC - Windows only
            choco install vlc $env:CHOCO_VLC_ARG --no-progress --limit-output
            # Install HTML Help Workshop - Windows only
            choco install html-help-workshop --no-progress --limit-output
          }
          else
          {
            # Install Mac only stuff
            # install dmgbuild tool
            python -m pip install --no-warn-script-location dmgbuild
            # use brew to build enchant, needed for pyenchant
            brew install enchant
          }
          # Get the packaging code
          Invoke-WebRequest -Uri "https://gitlab.com/openlp/packaging/-/archive/master/packaging-master.zip" -OutFile packaging-master.zip
          Expand-Archive -Path packaging-master.zip -DestinationPath .
          # If this is tag/replease we should also build the manual
          If ($env:APPVEYOR_REPO_TAG -eq $True -Or $env:FORCE_PACKAGING_MANUAL -eq 1) {
            python -m pip install --no-warn-script-location sphinx sphinx_rtd_theme
            Invoke-WebRequest -Uri "https://gitlab.com/openlp/documentation/-/archive/master/documentation-master.zip" -OutFile documentation-master.zip
            Expand-Archive -Path documentation-master.zip -DestinationPath .
            # If this is a release build, set release argument
            $releaseArg = ""
            If ($env:APPVEYOR_REPO_TAG -eq $True) {
                $releaseArg = "--release ""$env:APPVEYOR_REPO_TAG_NAME"""
            }
            cd packaging-master
            If ($isWindows) {
                $cmd = "python builders/windows-builder.py $releaseArg --skip-update -c windows/config-appveyor.ini -b ""$env:APPVEYOR_BUILD_FOLDER"" -d ../documentation-master --portable"
                iex $cmd
            } else {
                $cmd = "python builders/macosx-builder.py $releaseArg --skip-update -c osx/config-appveyor.ini -b ""$env:APPVEYOR_BUILD_FOLDER"" -d ../documentation-master"
                iex $cmd
            }
          } else {
            cd packaging-master
            If ($isWindows) {
                python builders/windows-builder.py --skip-update --skip-translations -c windows/config-appveyor.ini -b "$env:APPVEYOR_BUILD_FOLDER" --portable
            } else {
                python builders/macosx-builder.py --skip-update --skip-translations -c osx/config-appveyor.ini -b "$env:APPVEYOR_BUILD_FOLDER"
            }
          }
      }

artifacts:
  - path: dist\*.exe
    name: Windows Portable-installer
  - path: dist\*.msi
    name: Windows Installer
  - path: dist\*.dmg
    name: MacOSX Installer
